﻿#include <iostream>

class Animal
{

public:
    virtual void Voice()
    {
        std::cout << " ";
    }
};

class Dog : public Animal
{

public:

    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{

public:

    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{

public:

    void Voice() override
    {
        std::cout << "Muu!\n";
    }
};


int main()
{
    Animal* p[3] = { new Dog(), new Cat(), new Cow() };

    for (int i=0; i < 3; i++)
    {
        p[i]->Voice();
    }
   
}